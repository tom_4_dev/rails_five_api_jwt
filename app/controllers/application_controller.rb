class ApplicationController < ActionController::API
  include Response
  include ExceptionHandler

  # called before every action on controllers
  before_action :authorize_request
  attr_reader :current_user

  private

  def auth_present?
    !!request.env.fetch("HTTP_AUTHORIZATION", "").scan(/Bearer/).flatten.first
  end

  # Check for valid request token and return user
  def authorize_request
    @current_user = auth_present? ? (AuthorizeApiRequest.new(request.headers).call)[:user] : nil
  end
end
